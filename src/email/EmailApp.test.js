import React from 'react';
import { shallow } from 'enzyme';
import EmailApp from './EmailApp';

describe('<EmailApp/>', () => {

    it('should render correctly', () => {
        const component = shallow(<EmailApp />);
        expect(component).toMatchSnapshot();
    });
});