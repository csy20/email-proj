import React, {Component} from 'react';
import PropTypes from 'prop-types';
import EmailListItem from "./EmailListItem";
import './EmailList.scss';

export default class EmailList extends Component {

    render() {
        return (
            <div className="email-list">
                {this.props.data.collection.items.map(item => {
                    return <EmailListItem key={item.id} id={item.id} name={item.name} subjects={item.subjects}/>
                })}
            </div>
        )
    }
}

EmailList.propTypes = {
    data: PropTypes.object
};