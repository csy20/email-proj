import React from 'react';
import { shallow } from 'enzyme';
import EmailList from "./EmailList";

describe('<EmailList/>', () => {

    it('should render correctly', () => {

        const data = {
            collection: {
                items: [{
                    id: '1',
                    name: 'Item name 1',
                    subjects: ['Subject 1']
                }, {
                    id: '2',
                    name: 'Item name 2',
                    subjects: ['Subject 1', 'Subject 2']
                }]
            }
        };

        const component = shallow(<EmailList data={data}/>);
        expect(component).toMatchSnapshot();
    });
});