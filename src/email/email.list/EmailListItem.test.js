import React from 'react';
import { shallow } from 'enzyme';
import EmailListItem from "./EmailListItem";

describe('<EmailListItem/>', () => {

    it('should render correctly', () => {

        const data = {
            id: '1',
            name: 'Item name 1',
            subjects: ['Subject 1', 'Subject 2']
        };

        const component = shallow(<EmailListItem id={data.id} name={data.name} subjects={data.subjects}/>);
        expect(component).toMatchSnapshot();
    });
});