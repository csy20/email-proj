import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './EmailListItem.scss';

export default class EmailListItem extends Component {

    render() {
        return (
            <div className="email-list-item level">
                <div className="level-item level-left">
                    <h2 className="email-name">{this.props.name}</h2>
                    <div className="email-subjects">
                        {this.props.subjects.map((subject, i) => <span key={i} className="subject">{subject}</span>)}
                    </div>
                </div>
                <div className="level-item level-right">
                    <Link className="button is-medium" to={`/email/${this.props.id}`}>More</Link>
                </div>
            </div>
        );
    }
}

EmailListItem.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    subjects: PropTypes.array
};