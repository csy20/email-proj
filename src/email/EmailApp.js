import React, {Component} from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import withAsyncData from "../HOCs/WithAsyncData";
import EmailList from "./email.list/EmailList";
import EmailPreview from "./email.preview/EmailPreview";
import './EmailApp.scss';

const EmailListWithAsyncData = withAsyncData(
    EmailList,
    'http://localhost:3000/api/emails'
);

const EmailPreviewWithAsyncData = withAsyncData(
    EmailPreview,
    'http://localhost:3000/api/email'
);

export default class EmailApp extends Component {

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={EmailListWithAsyncData}/>
                    <Route path="/email/:id" component={EmailPreviewWithAsyncData}/>
                </Switch>
            </BrowserRouter>
        )
    }
}