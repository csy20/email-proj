import React from 'react';
import { shallow } from 'enzyme';
import EmailPreviewContent from "./EmailPreviewContent";

describe('<EmailPreviewContent/>', () => {

    const data = {
        text: 'Some text',
        html: '<h1>Some HTML</h1>'
    };

    it('should render correctly', () => {
        const component = shallow(<EmailPreviewContent content={data}/>);
        expect(component).toMatchSnapshot();
    });

    it('should change content on toggle of html/text buttons', () => {

        const component = shallow(<EmailPreviewContent content={data}/>);
        component
            .find('#button-text')
            .simulate('click');

        expect(component).toMatchSnapshot();

        component
            .find('#button-html')
            .simulate('click');

        expect(component).toMatchSnapshot();
    });
});