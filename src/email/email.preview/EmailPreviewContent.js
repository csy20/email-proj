import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class EmailPreviewContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            displayHtml: true
        }
    }

    handleHTMLClick() {
        this.setState({displayHtml: true});
    }

    handleTextClick() {
        this.setState({displayHtml: false});
    }

    render() {
        return (
            <div>
                <button id="button-html" className="button" onClick={this.handleHTMLClick.bind(this)}>HTML</button>
                <button id="button-text" className="button" onClick={this.handleTextClick.bind(this)}>Plain Text</button>
                <div className="email-body">
                    {this.state.displayHtml ? (
                        <iframe height="800px" width="100%" src={'data:text/html;charset=utf-8,' + encodeURI(this.props.content.html)}></iframe>
                    ) : (
                        <div>{this.props.content.text}</div>
                    )}
                </div>
            </div>
        )
    }
}

EmailPreviewContent.propTypes = {
    content: PropTypes.object
};