import React from 'react';
import { shallow } from 'enzyme';
import EmailPreview from "./EmailPreview";

describe('<EmailPreview/>', () => {

    it('should render correctly', () => {

        const data = {
            name: 'Email name',
            subjects: ['Subject 1', 'Subject 2'],
            body: {
                text: 'Some text',
                html: '<h1>Some HTML</h1>'
            }
        };

        const component = shallow(<EmailPreview data={data}/>);
        expect(component).toMatchSnapshot();
    });
});