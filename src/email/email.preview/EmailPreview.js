import React, {Component} from 'react';
import PropTypes from 'prop-types';
import EmailPreviewContent from "./EmailPreviewContent";

export default class EmailPreview extends Component {

    render() {
        return (
            <div>
                <div>
                    <h2>Message Name:</h2>
                    <span>{this.props.data.name}</span>
                </div>
                <div>
                    <h2>Subject Line:</h2>
                    <div>{this.props.data.subjects.map((subject, i) => <span key={i}>{subject}</span>)}</div>
                </div>
                <EmailPreviewContent content={this.props.data.body}/>
            </div>
        )
    }
}

EmailPreview.propTypes = {
    data: PropTypes.object
};