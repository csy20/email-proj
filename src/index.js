import React from 'react';
import ReactDOM from 'react-dom';
import EmailApp from "./email/EmailApp";

ReactDOM.render(
    <EmailApp/>,
    document.getElementById('email-app-root')
);