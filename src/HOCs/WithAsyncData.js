import React, {Component} from 'react';
import axios from "axios-jsonp-pro/index";

export default function withAsyncData(WrappedComponent, url) {

    return class extends Component {

        constructor(props) {
            super(props);
            this.state = {
                asyncData: null
            };
        }

        componentDidMount() {
            this._loadAsyncData();
        }

        componentWillUnmount() {
            if (this._asyncRequest) {
                // Can be replaced by a cancel function, if available.
                this._asyncRequest = null;
            }
        }

        render() {
            if (this.state.asyncData) {
                return (
                    <WrappedComponent data={this.state.asyncData} {...this.props} />
                )
            } else {
                return (
                    <div>Loading...</div>
                )
            }
        }

        _loadAsyncData() {

            // If an id parameter is available from the router
            const id = this.props.match.params.id || '';

            this._asyncRequest = axios.jsonp(`${url}/${id}`)
                .then(resp => {
                    this._asyncRequest = null;
                    this.setState({asyncData: resp});
                });
        }
    }
}