const express = require('express');
const app = express();

app.get('/api/emails', (req, res) => {

    const data = require('./data/emails.json');
    res.jsonp(data);
});

app.get('/api/email/:id', (req, res) => {

    const data = require(`./data/email-${req.params.id}.json`);
    res.jsonp(data);
});

app.listen(3000, () => console.log('Listening on port 3000'));