## Email Test Project

Simple Email SPA built with React. 

### Overview of approach:
* No boilerplate was used
* Stack: ES6/Babel, React, Parcel, Jest/Enzyme, Sass (w/ Bulma), Node/Express (for APIs)
* Uses hot module replacement for js and css
* Project runs Node/Express for APIs (purely for demonstration), but this would not normally be in the project (hence needing jsonp)

## Steps to run
* `npm i -g parcel-bundler`
* `npm i`
* `node ./api/index.js` (start the server for email apis)
* `npm start` (on http://localhost:1234)

## To test
* `npm test` (set to watcher mode)

## Build for prod
* `npm run build`

## Comments / To Be Completed

* Styling!! Barely started, choosing to use Bulma and importing styles on a component level.
* Testing: needs completing. I have added shallow snapshot testing for all components, but I haven't got to integration or E2E (using Jest and Puppeteer for E2E - happy to complete though :)  
* Routing does not deal with 404s
* No error handling of API responses
* Uses all of Bulma but might be a case for cherry picking
* API URLs are hardcoded - these should be in a separate file and use an environment variable to select the URL (i.e. dev, staging, prod)
* No active state for HTML vs Plain text button, and current boolean should be an enum (/const)
